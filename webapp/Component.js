sap.ui.define(
    ["sap/suite/ui/generic/template/lib/AppComponent"],
    function (Component) {
        "use strict";

        return Component.extend("bookingcustomer.Component", {
            metadata: {
                manifest: "json"
            }
        });
    }
);